import React from 'react';
var serialize = require('form-serialize');
var Dropzone = require('react-Dropzone');
var FormData = require('form-data');
var check_internet_connection;

export default class Messagefooter extends React.Component{
	constructor(props){
		super(props);
		this.state = {file : []}

	}

	message_sent(e){
		e.preventDefault()


				if(document.getElementById('fileupload').value){
					new Promise((resolve, reject)=>{
						let filedata = document.getElementById("fileupload");
				
						var formData = new FormData($('form')[0]);
						
						formData.append('message', document.getElementById("message").value);
						formData.append('receiverId', this.props.message_info.receiverId);
						formData.append('receiverType', "student");
						formData.append('access_token', this.props.access_token);
						formData.append('attachment', this.state.file)

						$.ajax({
				            url: `http://192.168.100.229/apdatasvc/public/api/chat/message/send`,
				            type: 'POST',
				            cache: false,
		        			processData: false,
		        			contentType: false,
						    
				            data: formData,
				           	success: function(result) {
				           		document.getElementById("fileupload").value = ""
				    			resolve({result : result})
		              			
		            		}.bind(this)
					        

				        })
					}).then((result)=>{

							let lastMessageId = this.props.lastMessageId;
									let connect_id = this.props.message_info.receiverId;
								
									let access_token = this.props.access_token
								$.get(`http://192.168.100.229/apdatasvc/public/api/mobile/chat/connection/message?access_token=${access_token}&connectionId=${connect_id}&lastMessageId=${lastMessageId}&isPolling=1`, (result)=>{
									var key = {}

									if(result.response){
										for(var i = 0 ; i< result.response.length; i++){

											if(result.response[i].fileType === "document"){
										    	key = {className : 'fa fa-file-word-o fa-lg', display_id: "display"}
										    	var obj = Object.assign(result.response[i], key);
											}
											else if(result.response[i].fileType === "image"){
											    key = {className : 'fa fa-file-picture-o fa-lg', display_id: "display"}
											    var obj = Object.assign(result.response[i], key);
											}
											else if(result.response[i].fileType === "pdf"){
												
											    key = {className : 'fa fa-file-pdf-o fa-lg', display_id: "display"}
											    var obj = Object.assign(result.response[i], key);
											}
											else{
												key = {className : 'other_chat'}
											    var obj = Object.assign(result.response[i], key);
											}
										}

									}

									else{
										document.getElementById('loadmore').style.display = "none"
									}

									this.props.thismessage.setState({messages: result.response})
									this.props.thismessage.setState({connectionId: this.props.message_info.receiverId})
									
								})
						})
					
				}

				else{


					new Promise((resolve, reject)=>{
						let filedata = document.getElementById("fileupload");
						
							var formData = new FormData($('form')[0]);
							
							formData.append('message', document.getElementById("message").value);
							formData.append('receiverId', this.props.message_info.receiverId);
							formData.append('receiverType', "student");
							formData.append('access_token', this.props.access_token);

							if(navigator.onLine){
								$.ajax({
						            url: `http://192.168.100.229/apdatasvc/public/api/chat/message/send`,
						            type: 'POST',
						            cache: false,
				        			processData: false,
				        			contentType: false,
								    
						            data: formData,
						           	success: function(result) {
						           		document.getElementById("message").value = ""
						           		resolve({result : result})
				              			
				            		}.bind(this)
							        

						        })
							}
							else{
								let className = {className : "network_class fa fa-paper-plane"}
								let local_obj = Object.assign(obj2, className)
								localStorage.setItem('local_obj', JSON.stringify(local_obj));

								var retrievedObject = JSON.parse(localStorage.getItem('local_obj'));

								$(window).on('load', function() {
								    localStorage.removeItem(retrievedObject);
								});

								
							}
						}).then((result)=>{

							let lastMessageId = this.props.lastMessageId;
									let connect_id = this.props.message_info.receiverId;
								
									let access_token = this.props.access_token
								$.get(`http://192.168.100.229/apdatasvc/public/api/mobile/chat/connection/message?access_token=${access_token}&connectionId=${connect_id}&lastMessageId=${lastMessageId}&isPolling=1`, (result)=>{
									var key = {}

									if(result.response){
										for(var i = 0 ; i< result.response.length; i++){

											if(result.response[i].fileType === "document"){
										    	key = {className : 'fa fa-file-word-o fa-lg', display_id: "display"}
										    	var obj = Object.assign(result.response[i], key);
											}
											else if(result.response[i].fileType === "image"){
											    key = {className : 'fa fa-file-picture-o fa-lg', display_id: "display"}
											    var obj = Object.assign(result.response[i], key);
											}
											else if(result.response[i].fileType === "pdf"){
												
											    key = {className : 'fa fa-file-pdf-o fa-lg', display_id: "display"}
											    var obj = Object.assign(result.response[i], key);
											}
											else{
												key = {className : 'other_chat'}
											    var obj = Object.assign(result.response[i], key);
											}
										}

									}

									else{
										document.getElementById('loadmore').style.display = "none"
									}

									this.props.thismessage.setState({messages: result.response})
									this.props.thismessage.setState({connectionId: this.props.message_info.receiverId})
									
								})
						})

					
				}	
    	
	}


	 onDrop(acceptedFiles, rejectedFiles) {
      acceptedFiles.forEach((file)=> {
          var fr = new FileReader();
            fr.onload = function(e) {
                console.log(e.target.result);
            };
         fr.readAsText(file);
        });
    }


	fileupload(e){
		let reader = new FileReader();
            let file = e.target.files[0];

            this.setState({file : file})
	}

	componentDidMount(){
		check_internet_connection = setInterval(this.check_internet_connection, 2000);
	}

	check_internet_connection(){
		if(navigator.onLine){
			document.getElementById('message').disabled = false;
			document.getElementById("message").placeholder = "Type a message";
		}
		else{
			document.getElementById('message').disabled = true;
			document.getElementById("message").placeholder = "Please check your Internet Connection";

		}
	}

	
	render(){

		return(
			<div className="messenger-content-footer">
						<div className="messenger-content-footer-inner">
							<div className="messenger-compose">
							<form action="http://192.168.100.229/apdatasvc/public/api/chat/message/send" id="form1" encType='multipart/form-data' method="post" onSubmit={this.message_sent}>
								<div className="messenger-compose-actions">
									<div className="messenger-compose-action">
										<label className="btn btn-link link-muted file-upload-btn"><span className="fa fa-paperclip"></span>
											<input className="file-upload-input fa fa-file" ref="theInput"  name="file" onChange = {this.fileupload.bind(this)} id="fileupload" accept=".jpeg, .jpg, .jpe, .jfif, .jif, .pdf, .svg, .doc, .docx, .xls, .ppt" type="file" name="messenger_compose_file" />
										</label>
									</div>			
									<div className="messenger-compose-action">
										<input type="submit" className="btn btn-link btn-send" onClick = {this.message_sent.bind(this)} value="Send" />
									</div>
								</div>
								<div className="messenger-compose-message">
									<textarea className="messenger-compose-input" name="text"  id="message"  placeholder="Type a message..."></textarea>
								</div>
							</form>
							</div>
						</div>
					</div>
			)
	}
}
