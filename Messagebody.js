import React from 'react';
var x;
var countdown

export default class Messagebody extends React.Component{
	constructor(props){
		super(props);
		this.state = {message : [],msg:[], connection_id : '',lastMessageId: "", list : false, pro : true};
		this.timer = this.timer.bind(this)

	}

/*	componentWillReceiveProps(){
		
		var connect_id = this.props.connectionid
		


		$.get(`http://192.168.100.229/apdatasvc/public/api/portal/chat/connection/message?access_token=2f1eed4e-66b7-49f1-98a5-e547c5567ed2&connectionId=${connect_id}`, (result)=>{
			
			var key = {}
			for(var i = 0 ; i< result.response.length; i++){

				if(result.response[i].fileType === "document"){
			    	key = {className : 'fa fa-file-word-o fa-lg', display_id: "display"}
			    	var obj = Object.assign(result.response[i], key);
				}
				else if(result.response[i].fileType === "image"){
				    key = {className : 'fa fa-file-picture-o fa-lg', display_id: "display"}
				    var obj = Object.assign(result.response[i], key);
				}
				else if(result.response[i].fileType === "pdf"){
					
				    key = {className : 'fa fa-file-pdf-o fa-lg', display_id: "display"}
				    var obj = Object.assign(result.response[i], key);
				}
				else{
					key = {className : 'other_chat'}
				    var obj = Object.assign(result.response[i], key);
				}
			}



			this.setState({msg : result.response})
			this.setState({lastMessageId : result.response[0].id})
		})

		this.setState({connection_id : this.props.connectionid})


	}*/

	componentDidMount(){
		
		countdown = setInterval(this.timer, 2000);
	}

	timer(){
		
		var connect_id = this.props.connectionid
		let lastMessageId = this.props.lastMessageId-1
		let access_token = this.props.access_token
		if(lastMessageId && connect_id){
			$.get(`http://192.168.100.229/apdatasvc/public/api/mobile/chat/connection/message?access_token=${access_token}&connectionId=${connect_id}&lastMessageId=${lastMessageId}`, (result)=>{
			var key = {}
	
			console.log("data")
			for(var i = 0 ; i< result.response.length; i++){

				if(result.response[i].fileType === "document"){
			    	key = {className : 'fa fa-file-word-o fa-lg', display_id: "display"}
			    	var obj = Object.assign(result.response[i], key);
				}
				else if(result.response[i].fileType === "image"){
				    key = {className : 'fa fa-file-picture-o fa-lg', display_id: "display"}
				    var obj = Object.assign(result.response[i], key);
				}
				else if(result.response[i].fileType === "pdf"){
					
				    key = {className : 'fa fa-file-pdf-o fa-lg', display_id: "display"}
				    var obj = Object.assign(result.response[i], key);
				}
				else{
					key = {className : 'other_chat'}
				    var obj = Object.assign(result.response[i], key);
				}
			}
			
			this.setState({msg : result.response})
			this.props.this.setState({pro : true})
		})
		}

		
	}



	othermessages(){

		var lastMessageId;
		
		if(this.props.pro == true){
			console.log("state",this.state.msg)
			lastMessageId = this.state.msg[0].id
		}
		else{
			console.log("props")
			lastMessageId = this.props.messages[0].id	
		}

		this.setState({pro : false})
		this.setState({list : true})

		var connect_id = this.props.connectionid
		let access_token = this.props.access_token

		$.get(`http://192.168.100.229/apdatasvc/public/api/mobile/chat/connection/message/old?access_token=${access_token}&connectionId=${connect_id}&lastMessageId=${lastMessageId}&isPolling=1`, (result)=>{
			var key = {}
		
			if(result.response){
				for(var i = 0 ; i< result.response.length; i++){

					if(result.response[i].fileType === "document"){
				    	key = {className : 'fa fa-file-word-o fa-lg', display_id: "display"}
				    	var obj = Object.assign(result.response[i], key);
					}
					else if(result.response[i].fileType === "image"){
					    key = {className : 'fa fa-file-picture-o fa-lg', display_id: "display"}
					    var obj = Object.assign(result.response[i], key);
					}
					else if(result.response[i].fileType === "pdf"){
						
					    key = {className : 'fa fa-file-pdf-o fa-lg', display_id: "display"}
					    var obj = Object.assign(result.response[i], key);
					}
					else{
						key = {className : 'other_chat'}
					    var obj = Object.assign(result.response[i], key);
					}
				}

			}

			else{
				document.getElementById('loadmore').style.display = "none"
			}


			if(this.state.msg.length > 0){
				let other = result.response;
				let old = this.state.msg;
				var all_messages = other.concat(old);
			
				this.setState({msg : all_messages})
				this.props.this.setState({lastMessageId : all_messages[0].id, pro : true})
				this.setState({list : true})
			}
			else{
				let other = result.response;
				let old = this.props.messages;
				var all_messages = other.concat(old);
				
				this.setState({msg : all_messages})
				this.props.this.setState({lastMessageId : all_messages[0].id, pro : true})
				this.setState({list : true})
			}


			
		})
	}



	render(){
		var message_list

		if(this.props.messages && this.props.pro == false){
			message_list = this.props.messages.map((res, index)=>{
				
				return(
					<li className="conversation-item" key = {index}>
						<div className={res.direction} id="direction" title={res.direction}><input type="hidden" className="direction" id={res.id} />
							<div className="conversation-avatar"><img className="rounded" width="36" height="36" src="assets/images/messenger-img.jpg" alt="" /> </div>
								<div className="conversation-messages">
								<div className="conversation-message">{res.content}</div>
										
								<div className="conversation-timestamp">{res.sentAt}</div>
							</div>
						</div>
					</li>
					
					)
			})

		}

		else if(this.state.msg && this.props.pro == true){
			message_list = this.state.msg.map((res, index)=>{

				return(
					<li className="conversation-item" key = {index}>
						<div className={res.direction} id="direction" title={res.direction}><input type="hidden" className="direction" id={res.id} />
							<div className="conversation-avatar"><img className="rounded" width="36" height="36" src="assets/images/messenger-img.jpg" alt="" /> </div>
							<div className="conversation-messages">
								<div className="conversation-message"><a target="_blank" href={res.content} className={res.className}></a><span id={res.display_id} >{res.content}</span></div>
										
								<div className="conversation-timestamp">{res.sentAt}</div>
							</div>
						</div>
					</li>
					
					)
			})

		}

		else{
			return(
				<div></div>
			)
		}

		return(

			<div className="messenger-content-body" id="content1">
						<div className="messenger-content-body-inner">
							<div className="messenger-scrollable-content" id="scroll" >
								<ul className="conversation">
									<center><a onClick = {this.othermessages.bind(this)} id="loadmore">Load more</a></center>
									{message_list}
								</ul>
							</div>
						</div>
					</div>
			)
	}


}
