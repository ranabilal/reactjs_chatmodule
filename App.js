
import lodash from 'lodash'
import faker from 'faker'
import React, { Component } from 'react'
import { Search, Grid, Header } from 'semantic-ui-react'
import asyncPoll from 'react-async-poll';
import Messagecontent from './message-content';
import Messagebody from './Messagebody';
import Messagefooter from './Messagefooter';
var access_token = "d397451d-3941-4812-992d-91a6644d20b1";

export default class App extends React.Component {
	constructor(props){
		super(props);
		this.state = {lastMessageId : "", pro : false, message_connections : [],access_token : '', messages : "",loadmore: false, footer: false, result: [], loadmore: false, result1:[], connection: "", connectionId: "", recent_connection: [], showresult: "false"}
		this.setState({access_token : access_token})
		this.timer = this.timer.bind(this)
		this.handleSearchChange = this.handleSearchChange.bind(this)
	}

	
	componentWillMount(){
		
		
		$.get(`http://192.168.100.229/apdatasvc/public/api/portal/chat/connection/active?access_token=${access_token}`, (result)=>{	
			
			this.setState({message_connections : result.response.data, message_sentData : false})

		})

		
	}

	resetComponent(){this.setState({ isLoading: false, results: [], value: '' })}
	
	handleResultSelect(e, result){this.setState({ value: result.title })}

	handleSearchChange(e, value){
		
	    this.setState({ isLoading: true, value })
	    let search = value
	    $.get(`http://192.168.100.229/apdatasvc/public/api/portal/chat/connection/active?access_token=${access_token}&search=${search}&per_page&current_page`, (result)=>{	
			
			this.setState({message_connections : result.response.data})

		})
	    setTimeout(() => {
	      if (this.state.value.length < 1) return this.resetComponent()

	      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
	      const isMatch = (result) => re.test(result.title)
	    }, 500)
	  }

	componentDidMount(){
		this.countdown = setInterval(this.timer, 5000);
		$(".header").remove();
	}

	timer(){

		$.get(`http://192.168.100.229/apdatasvc/public/api/portal/chat/connection/recent/active?access_token=${access_token}&isPolling=1`, (result)=>{
			this.setState({recent_connection : result.response})
		
		})
	}

	onPollInterval(props, dispatch) {
   
 
	}

	click(e){
		let connect_id = e.target.id
		this.setState({connectionId : connect_id})
		this.setState({footer : true, pro : false})
		var obj = {
			receiverId : e.target.id,
			receiverType : "student",
			access_token : "2f1eed4e-66b7-49f1-98a5-e547c5567ed2"
		}

		this.setState({message_sentData : obj})

		$.get(`http://192.168.100.229/apdatasvc/public/api/portal/chat/connection/message?access_token=${access_token}&connectionId=${connect_id}`, (result)=>{
			var key = {}
			for(var i = 0 ; i< result.response.length; i++){

				if(result.response[i].fileType === "document"){
			    	key = {className : 'fa fa-file-word-o fa-lg', display_id: "display"}
			    	var obj = Object.assign(result.response[i], key);
				}
				else if(result.response[i].fileType === "image"){
				    key = {className : 'fa fa-file-picture-o fa-lg', display_id: "display"}
				    var obj = Object.assign(result.response[i], key);
				}
				else if(result.response[i].fileType === "pdf"){
					
				    key = {className : 'fa fa-file-pdf-o fa-lg', display_id: "display"}
				    var obj = Object.assign(result.response[i], key);
				}
				else{
					key = {className : 'other_chat'}
				    var obj = Object.assign(result.response[i], key);
				}
			}
			this.setState({messages : result.response, connection : obj, lastMessageId : result.response[0].id})
			
		})
	}

	others(e){
		$.get(`http://192.168.100.229/apdatasvc/public/api/portal/chat/connection/recent/active?access_token=${access_token}&isPolling=1`, (result)=>{
			this.setState({recent_connection : result.response})
		
		})


	}

   render() {

   	var mythis = this;
   	var data;
   	var recent;

   		if(this.state.message_connections){
   		 data = this.state.message_connections.map((res, index)=>{
  
   				return(
   					<li className="messenger-list-item message" id={res.connect_id} key = {index} onClick = {this.click.bind(this)}> <a className="messenger-list-link" href="#0601274412" data-toggle="tab">
						<div className="messenger-list-avatar" id={res.connect_id}> <img id={res.connect_id} width="40" height="40" src={res.avatar} alt="" /> </div>
							<div className="messenger-list-details">
								
								<div className="messenger-list-name" id ={res.connect_id}>{res.name}</div>
								
								<div className="messenger-list-message" id={res.connect_id}> <small id={res.connect_id} className="truncate">Curabitur vel mi ante.</small> </div>
							</div>
							</a> 
					</li>
   					)
   			})

   		}

   		else if(this.state.recent_connection ){
   			recent = this.state.recent_connection.map((res, index)=>{
  
   				return(
   					<li className="messenger-list-item message" id={res.connect_id} key = {index} onClick = {this.click.bind(this)}> <a className="messenger-list-link" href="#0601274412" data-toggle="tab">
						<div className="messenger-list-avatar"> <img width="40" height="40" src={res.avatar} alt="" /> </div>
							<div className="messenger-list-details">
								
								<div className="messenger-list-name" id ={res.connect_id}>{res.name}</div>
								
								<div className="messenger-list-message"> <small className="truncate">Curabitur vel mi ante.</small> </div>
							</div>
							</a> 
					</li>
   					)
   			})

   		}


      return (
           			<div className="messenger-layout-main">
						<div className="messenger-layout-content">
							<div className="messenger">
								<div id="contacts" className="messenger-sidebar active">
									<div className="messenger-sidebar-header">
										<div className="title-bar">
											<h1 className="title-bar-title"> <span className="d-ib">Messenger</span> <span className="d-ib"> <a className="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip"> <span className="sr-only">Add to shortcut list</span> </a> </span> </h1>
										</div>
										 <Grid>
									        <Grid.Column width={8}>
										          <Search
										            loading={this.state.isLoading}
										            onResultSelect={this.handleResultSelect}
										            onSearchChange={this.handleSearchChange}
										            results={this.state.results}
										            value={this.state.value}
										          />
									          </Grid.Column>
									      </Grid>
									</div>
									<div className="messenger-sidebar-body" >
										<div className="custom-scrollbar">
										<div className="container"></div>
											<div id="exTab2" >	
												<ul className="nav nav-tabs">
													<li className="active">
											        	<a  href="#1" data-toggle="tab">All Messages</a>
													</li>
													<li onClick = {this.others.bind(this)}>
														<a href="#2" data-toggle="tab">Recent</a>
													</li>
												</ul>

												<div className="tab-content ">
													<div className="tab-pane active" id="1">
											            <ul className="messenger-list">
															{data}
														</ul>
													</div>
													<div className="tab-pane" id="2">
											        	<ul className="messenger-list">
															{recent}
														</ul>
													</div>
												</div>
											</div>
												
										</div>
									</div>
								</div>

								<div id="0601274412" className="messenger-content active">
									<div className="messenger-content-inner">
										
										<Messagebody lastMessageId = {this.state.lastMessageId} pro = {this.state.pro} footer={this.state.footer} access_token = {access_token} loadmore = {this.state.loadmore} messages = {this.state.messages} this={this} data = {this.props} connectionid = {this.state.connectionId}/>
										<Messagefooter pro = {this.state.pro} lastMessageId = {this.state.lastMessageId} access_token = {access_token} message_info={this.state.message_sentData} thismessage={mythis} connection = {this.state.connection} result={this.state.result}/>
									</div>
								</div>
							</div>
						</div>
					</div>
      );
   }
}